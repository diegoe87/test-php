<?php
/**
 * @author           Diego Cortés <diegoe87@gmail.com>
 * @link             http://diegocortes.co
 *
 * Global configuration
 */

final class Config {
  // Database host
  const DB_HOST = '127.0.0.1';

  // Database name
  const DB_NAME = 'test-php';

  // Database user
  const DB_USR = 'root';

  // Database password
  const DB_PWD = 'admin';

  // Default controller 
  const DEFAULT_CONTROLER = 'Contact';

  // Site URL
  const SITE_URL = 'http://localhost/test-php/';
}