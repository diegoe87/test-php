<?php
/**
 * @author           Diego Cortés <diegoe87@gmail.com>
 * @link             http://diegocortes.co
 * 
 * Controller
 */
require_once "Core/View.php";
require_once "App/Model/Contact.php";

class ContactController {

    /**
     * Load forms view
     */
    public function form(){
        View::renderTemplate("contact_form");
    }

    /**
     * Save a contact and load forms view
     */
    public function save() {
        $oContact = new Contact();
        $response = array();

        if( $_POST ) {
            $params = array();
            $params["name"] = $_POST["name"];
            $params["email"] = $_POST["email"];
            $params["message"] = $_POST["message"];
            $params["created_dtm"] = date( 'Y-m-d H:i:s', time() );;
            
            if( $oContact->add($params) ) {
                $response['success'] = true;
            } else {
                $response['success'] = true;
            }
        }
        View::renderTemplate("contact_form", $response);
    }

    /**
     * Get all the contacts to JSON format
     */
    public function getAll() {
        $temp = new Contact();
        echo json_encode( $temp->getAll() );
    }
    
    /**
     * 
     */
    public function error404(){
        View::renderTemplate("404");
    }

}