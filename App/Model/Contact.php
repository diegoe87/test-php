<?php
/**
 * @author           Diego Cortés <diegoe87@gmail.com>
 * @link             http://diegocortes.co
 * Contact Model
 */
require_once "Core/Db.php";

class Contact {
    protected $db;

    public function __construct() {
        $this->db = Db::getInstance();
    }

    /**
     * Get all the records of contacts
     */
    public function getAll() {
        $query = $this->db->query('SELECT * FROM contact ORDER BY created_dtm DESC');
        return $query->fetchAll(\PDO::FETCH_OBJ);
    }

    /**
     * Save the contact information
     */
    public function add(array $data) {
        $query = $this->db->prepare('INSERT INTO contact (name, email, message, created_dtm) VALUES(:name, :email, :message, :created_dtm)');
        return $query->execute($data);
    }

    /**
     * Get a specific contact by id
     */
    public function getById($id) {
        $query = $this->db->prepare('SELECT * FROM contact WHERE id = :contactId LIMIT 1');
        $query->bindParam(':contactId', $id, \PDO::PARAM_INT);
        $query->execute();
        return $query->fetch(\PDO::FETCH_OBJ);
    }

    /**
     * Update contact
     */
    public function update(array $data) {
        $query = $this->db->prepare('UPDATE contact SET name = :name, email = :email, message= :message WHERE id = :contactId LIMIT 1');
        $query->bindValue(':contactId', $data['id'], \PDO::PARAM_INT);
        $query->bindValue(':name', $data['name']);
        $query->bindValue(':email', $data['email']);
        $query->bindValue(':message', $data['message']);
        return $query->execute();
    }

    /**
     * Delete contact
     */
    public function delete($id) {
        $query = $this->db->prepare('DELETE FROM contact WHERE id = :contactId LIMIT 1');
        $query->bindParam(':contactId', $id, \PDO::PARAM_INT);
        return $query->execute();
    }
}