<?php
/**
 * @author           Diego Cortés <diegoe87@gmail.com>
 * @link             http://diegocortes.co
 */
?>
<section id="section-form">
  <div class="container">    
  <h2>Contact</h2>
    <form action="<?= Config::SITE_URL; ?>?c=contact&a=save" method="POST">
      <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name" required>
      </div>
      <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="email" name="email" required>
      </div>
      <div class="form-group">
        <label for="message">Message</label>
        <textarea class="form-control" id="message" name="message"></textarea>
      </div>
      <button type="submit" class="btn btn-default">Submit</button>
    </form> 
    <?php if( isset($success) && $success ): ?>
      <div class="alert alert-success">
        <strong>Success!</strong> Thank you for registering!
      </div>
    <?php elseif( isset($success) ): ?>
      <div class="alert alert-danger">
        <strong>Error</strong> Please try again.
      </div>  
    <?php endif; ?>
  </div>
</section>

<section id="section-ajax">
  <div class="container">  
      <div id="result-ajax">
       -----
      </div>
      <button id="loadContacts" type="botton" class="btn btn-default">Load Contacts via AJAX</button>
  </div>
</section>