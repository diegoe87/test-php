<?php
/**
 * @author           Diego Cortés <diegoe87@gmail.com>
 * @link             http://diegocortes.co
 *
 * Database Conection with pattern Singleton 
 */

class Db extends \PDO
{
  // Connection instance
  private static $instance = null;

  public function __construct()
  {
    $aDriverOptions[\PDO::MYSQL_ATTR_INIT_COMMAND] = 'SET NAMES UTF8';
    parent::__construct('mysql:host=' . Config::DB_HOST . ';dbname=' . Config::DB_NAME . ';', Config::DB_USR, Config::DB_PWD, $aDriverOptions);
    $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
  }
  
  public static function getInstance()
  {
    if( self::$instance == null )
    {
      self::$instance = new self();
    }
    return self::$instance;
  }
}