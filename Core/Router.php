<?php
/**
 * @author           Diego Cortés <diegoe87@gmail.com>
 * @link             http://diegocortes.co
 */

require_once "App/Config.php";

class Router {

  /**
   * Load a specific Controller and execute the action
   */
  public static function route($controller, $action) {
    $nameClassController = ucwords($controller)."Controller";
    $pathFileController = dirname(__DIR__) . "App/Controller/".$nameClassController.".php";
    
    if( !is_readable($pathFileController) ){
      $strFileController = "App/Controller/".ucwords(Config::DEFAULT_CONTROLER).'Controller.php';   
    }

    require_once $strFileController;
    $controllerObj = new $nameClassController();

    if( method_exists($controllerObj, $action) ) {
      call_user_func( array( $controllerObj, $action ) );
    } else {
      call_user_func( array( $controllerObj, 'error404' ) );
    }
  }

}