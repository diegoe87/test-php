<?php
/**
 * @author           Diego Cortés <diegoe87@gmail.com>
 * @link             http://diegocortes.co
 * Controller de
 */

class View {

  /**
   * Render a view file
   */
  public static function renderTemplate($view, $args = []) {
    extract($args, EXTR_SKIP);

    $file = dirname(__DIR__) . "/App/View/$view.php";

    if (!is_readable($file)) {
        $file = dirname(__DIR__) . "/App/View/404.php";
    }
    require dirname(__DIR__) . "/App/View/Include/header.php";
    require $file;
    require dirname(__DIR__) . "/App/View/Include/footer.php";
  }
}