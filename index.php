<?php
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);

  require_once 'Core/Router.php';

  if ( isset($_GET['c']) && isset($_GET['a']) ) {
      $controller = $_GET['c'];
      $action     = $_GET['a'];
  } else {
      $controller = 'contact';
      $action     = 'form';
  }

  Router::route($controller, $action);
