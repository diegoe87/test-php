$(document).ready(function(){
  
  // Load contacts via ajax
  $('#loadContacts').click(function(){
    $.ajax( { url: SITE_URL, data: {'c':'contact', 'a':'getall'}, dataType: "json"} )
    .done(function(data) {
      var rowsHtml = '<table>';
      $(data).each(function(i, v){
        rowsHtml += buildHtml(v);
      });
      rowsHtml += '</table>';
      $('#result-ajax').html(rowsHtml);
    })
    .fail(function() {
      alert( "Error" );
    });
  });
  
});

// Generate html row
function buildHtml(_obj) {
  var resultHtml = '<tr>';
  
  resultHtml += '<td>'+_obj.id+'</td>';
  resultHtml += '<td>'+_obj.name+'</td>';
  resultHtml += '<td>'+_obj.email+'</td>';
  resultHtml += '<td>'+_obj.created_dtm+'</td>';
  
  resultHtml += '</tr>';
  
  return resultHtml;
}